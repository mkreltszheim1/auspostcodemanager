A simple .NET application using AngularJS to retrieve Australian suburb names from a JSON file via a postcode lookup.

Suburb names can also be updated after they have been retrieved.