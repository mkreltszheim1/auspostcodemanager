﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AusPostcodeManager.Helpers
{
    public class Constants
    {
        public const string JsonFilename = "aus-postcodes.json";
        public static string JsonFullPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("\\Content\\"), JsonFilename);
    }
}