﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AusPostcodeManager.Services;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using AusPostcodeManager.Helpers;
using AusPostcodeManager.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace AusPostcodeManager.Controllers
{
    public class PostcodeController : ApiController
    {
        private IPostcodeService _service;

        public PostcodeController()
        {
            _service = new PostcodeService();
        }

        // GET: api/Postcode
        public JArray Get()
        {
            throw new NotImplementedException();
        }

        // GET: api/Postcode/5
        public List<string> Get(int id)
        {
            // if postcode is invalid return empty result
            if (!_service.IsValidPostcode(id))
            {
                return new List<string>();
            }

            // get postcode data from JSON file
            var postcodes = _service.GetPostcodeDataFromJsonFile(Constants.JsonFullPath);

            // get data for the postcode and return ordered list of suburbs 
            var postcodeModels = (List<PostcodeModel>)postcodes.ToObject(typeof(List<PostcodeModel>));
            return postcodeModels.Where(p => p.Postcode == id).OrderBy(s=>s.Suburb).Select(p => p.Suburb).ToList();
        }

        // POST: api/Postcode
        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // PUT: api/Postcode/5
        //public void Put(int postcode, string suburbs)
        public HttpResponseMessage Put(int postcode, string suburbs)
        {
            // retrieve current data from JSON file
            var postcodes = _service.GetPostcodeDataFromJsonFile(Constants.JsonFullPath).ToObject<List<PostcodeModel>>();

            // Deserialise suburb list
            var suburbsList = JsonConvert.DeserializeObject<List<string>>(suburbs);

            // if any suburb names are empty, abort the request as the validation has failed
            if (suburbsList.Any(s => s.IsNullOrWhiteSpace()))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            // prepare updated data
            postcodes = _service.UpdateSuburbs(postcode, postcodes, suburbsList);

            // update JSON file
            _service.WriteToJsonFile(postcodes, Constants.JsonFullPath);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
        
        // DELETE: api/Postcode/5
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
