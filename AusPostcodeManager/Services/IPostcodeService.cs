﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AusPostcodeManager.Models;
using Newtonsoft.Json.Linq;

namespace AusPostcodeManager.Services
{
    interface IPostcodeService
    {
        JArray GetPostcodeDataFromJsonFile(string jsonFullPath);

        bool IsValidPostcode(int id);

        void WriteToJsonFile(List<PostcodeModel> postcodes, string jsonFullPath);

        List<PostcodeModel> UpdateSuburbs(int postcode, List<PostcodeModel> postcodes, List<string> objsuburbs);
    }
}
