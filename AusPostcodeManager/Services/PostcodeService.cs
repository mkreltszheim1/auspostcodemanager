﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AusPostcodeManager.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AusPostcodeManager.Services
{
    public class PostcodeService : IPostcodeService
    {
        public JArray GetPostcodeDataFromJsonFile(string jsonFullPath)
        {
            using (var streamReader = new StreamReader(jsonFullPath))
            {
                return JArray.Parse(streamReader.ReadToEnd());
            }
        }

        public bool IsValidPostcode(int id)
        {
            return (id >= 100 && id <= 9999);
        }

        public void WriteToJsonFile(List<PostcodeModel> postcodes, string jsonFullPath)
        {
            var jsonPostCodes = JsonConvert.SerializeObject(postcodes);
            using (StreamWriter file = File.CreateText(jsonFullPath))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                writer.WriteRaw(jsonPostCodes);
            }
        }

        public List<PostcodeModel> UpdateSuburbs(int postcode, List<PostcodeModel> postcodes, List<string> suburbs)
        {
            // remove exisitng entries for this postcode
            postcodes.RemoveAll(p => p.Postcode == postcode);

            // add new entries for this postcode
            foreach (var suburb in suburbs)
            {
                postcodes.Add(new PostcodeModel() { Postcode = postcode, Suburb = suburb });
            }
            return postcodes;
        }
    }
}