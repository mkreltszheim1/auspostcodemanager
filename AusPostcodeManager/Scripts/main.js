﻿angular.module('postcodeApp', [])
.controller('postcodeCtrl', function ($scope, $http) {
    $scope.GetSuburbs = function (postcode) {
        $scope.postcode = postcode;
        $http({
            method: 'GET',
            url: '/Api/Postcode/'+postcode//,
            //data: { id: postcode }
        }).then(function successCallback(result) {
            if (result && result.data) {
                $scope.suburbs = result.data;
            } else {
                $scope.suburbs = null;
            }
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
    $scope.UpdateSuburbs = function (postcode, suburbs) {
        suburbs = JSON.stringify(suburbs);
        $http({
            method: 'PUT',
            url: '/Api/Postcode/',
            params: { postcode, suburbs  },
            contentType: "application/json"
        }).then(function successCallback(result) {
            debugger;
            if (result && result.data) {
                $scope.suburbs = result.data;
            } else {
                $scope.suburbs = null;
            }
        }, function errorCallback(response) {
            debugger;
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
});