﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AusPostcodeManager.Models
{
    public class PostcodeModel
    {
        public string Suburb { get; set; }

        [Range(100, 9999)]
        public int Postcode { get; set; }
    }
}